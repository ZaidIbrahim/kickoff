package com.emerio.slot.controller;

import com.emerio.slot.entity.Person;
import com.emerio.slot.repository.PersonRepository;
import com.google.common.collect.Lists;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonController
{

    @Autowired
    private PersonRepository personRepository;


    @RequestMapping(method = RequestMethod.GET, path = "/api/list", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Document> list()
    {
        List<Person> alldata = Lists.newArrayList(this.personRepository.findAll());
        Long count = this.personRepository.count();
        Document output = new Document().append("min", 0).append("max", count-1).append("data",alldata);
        return new ResponseEntity<>(output, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/import", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Document> importXlsxFile (@RequestParam("file") MultipartFile file) {
    {
        try
        {
            this.personRepository.deleteAll();

            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheet = workbook.getSheetAt(0);

            for (int i =0; i < sheet.getLastRowNum(); i++)
            {
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(0);
                Person person = new Person(cell.getStringCellValue());
                this.personRepository.save(person);
            }

            Document output = new Document().append("status","oke");

            return  new ResponseEntity<>(output, HttpStatus.OK);

        } catch (Exception e)
        {
            Document output = new Document().append("status","failed");
            return  new ResponseEntity<>(output, HttpStatus.PRECONDITION_FAILED);
        }

        }
    }





}
