package com.emerio.slot.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Person
{

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    public Person(Long id, String username) {
        this.username = username;
        this.id = id;
    }

    public Person(String username) {
        this.username = username;
    }


    public Person() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
