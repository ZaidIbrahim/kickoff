$(document).ready(function() {
  // Config
  var apiUrl = 'http://localhost:3000/data';
  var rouletteTimer = 30;

  var dataLength;

  // Element
  var list = $('.list');
  var liHeight = $('li').outerHeight();
  var btnSpin = $('#slot-trigger');
  var head = $('#head');
  var stick = $('#stick');
  var hole = $('#hole');

  var randomButton = $('#randomButton');

  // Listen To Spacebar or Enter
  $(function() {
    $(document).on('keypress', function(event) {
      switch (event.which) {
        case 13:
          slotTriggerMove();
          spin();
          break;

        case 32:
          getData();
          break;

        default:
          break;
      }
    });
  });

  getData();

  function getData() {
    $.ajax({
      url: apiUrl,
      contentType: 'application/json',
      dataType: 'json',
      success: function(result) {
        dataLength = result.length;

        var arrayShuffled = shuffle(result);

        $.map(arrayShuffled, function(value) {
          $('.list').append('<li>' + value.name + '</li>');
        });
        TweenMax.to(list, 1, {
          y: `-=${liHeight * Math.floor(dataLength / 2)}px`,
        });
      },
    });
  }

  function shuffle(array) {
    var currentIndex = array.length;
    var temporaryValue;
    var randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  function spin() {
    var r1 = R(1, dataLength);

    TweenMax.to(list, rouletteTimer, {
      y: 1 - liHeight * r1,
      // ease: Elastic.easeOut.config(5, 0.1),
      // ease: Bounce.easeOut,
      ease: Circ.easeInOut,
    });
  }

  function slotTriggerMove() {
    TweenMax.set([head, stick, hole], { y: 0, scale: 1 });
    TweenMax.to(head, 0.4, { y: 70, repeat: 1, yoyo: true, ease: Sine.easeIn });
    TweenMax.to(stick, 0.4, {
      y: 14,
      scaleY: 0.3,
      transformOrigin: '50% 100%',
      repeat: 1,
      yoyo: true,
      ease: Sine.easeIn,
    });
    TweenMax.to(hole, 0.4, {
      y: 10,
      scaleY: 2,
      repeat: 1,
      yoyo: true,
      ease: Sine.easeIn,
    });
  }

  //Generate random spin times
  function R(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  // Button
  btnSpin.click(function() {
    slotTriggerMove();
    spin();
  });

  randomButton.click(function() {
    getData();
  });
});
